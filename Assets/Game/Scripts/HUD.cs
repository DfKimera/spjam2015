﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUD : MonoBehaviour {

	public GameObject player;
	
	protected Text uiCredits;
	protected Text uiHealth;
	protected Text uiQuests;
	
	protected Button uiQuestBtn;
	
	protected PlayerController playerCtrl;

	// Use this for initialization
	void Start () {
		playerCtrl = player.GetComponent<PlayerController>();
		
		uiCredits = transform.Find("Credits").gameObject.GetComponent<Text>();
		uiHealth = transform.Find("Health").gameObject.GetComponent<Text>();
		uiQuestBtn = transform.Find("QuestBtn").gameObject.GetComponent<Button>();
		uiQuests = uiQuestBtn.gameObject.transform.Find("Count").gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		uiCredits.text = "$ " + playerCtrl.credits.ToString() + "g";
		uiHealth.text = playerCtrl.health.ToString() + "hp";
	}
}
