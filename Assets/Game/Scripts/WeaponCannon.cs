﻿using UnityEngine;
using System.Collections;

public class WeaponCannon : MonoBehaviour {
	
	public GameObject model;
	public int damage = 25;
	
	public float fireRate = 1.2f; 
	public float cooldown = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		cooldown--;
		
		if(cooldown < 0) {
			cooldown = 0;
		}
	}
	
	public void Fire(Vector3 direction) {
		
		if(cooldown <= 0) {
			Debug.Log("Firing @ " + direction);
			
			GameObject projectile = Instantiate(model);
			Physics2D.IgnoreCollision(projectile.GetComponent<Collider2D>(), GetComponent<Collider2D>());
			projectile.GetComponent<Projectile>().FireFrom(gameObject, damage, direction * -1);
			
			cooldown = 100.0f / fireRate;
		}
		
	}
}
