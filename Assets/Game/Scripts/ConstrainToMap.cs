﻿using UnityEngine;
using System.Collections;

public class ConstrainToMap : MonoBehaviour {

	public Vector2 min = new Vector2(-1300, -420);
	public Vector2 max = new Vector2(-1040, -130);

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		Vector3 pos = transform.position;
		
		if(pos.x < min.x) {
			pos.x = max.x;
		} else if(pos.x > max.x) {
			pos.x = min.x;
		}
		
		if(pos.y < min.y) {
			pos.y = max.y;
		} else if(pos.y > max.y) {
			pos.y = min.y;
		}
		
		transform.position = pos;
		
	}
}
