using UnityEngine;
using System;
using System.Xml;

public class Quest {

	public int id;
	public String title;
	public String description;
	public int reward;
	public String success;
	public String npc;
	
	public static Quest fromXMLNode(XmlNode node) {
		Quest m = new Quest();
		
		m.id = Convert.ToInt16(node.SelectSingleNode("id").InnerText);
		m.title = node.SelectSingleNode("title").InnerText;
		m.description = node.SelectSingleNode("description").InnerText;
		m.reward = Convert.ToInt32(node.SelectSingleNode("reward").InnerText);
		m.success = node.SelectSingleNode("success").InnerText;
		m.npc = node.SelectSingleNode("npc").InnerText;
		
		return m;
	}
	
}