﻿using UnityEngine;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameData : MonoBehaviour {
	
	protected static System.Random randomizer = new System.Random();
	
	public static Dictionary<int, Quest> quests = new Dictionary<int, Quest>();

	void Start () {
		LoadQuests();
	}
	
	void LoadQuests() {
		Debug.Log("Loading quests...");
		
		XmlDocument doc = new XmlDocument();		
		doc.LoadXml( Resources.Load<TextAsset>("Quests").text );
		
		foreach(XmlNode node in doc.SelectNodes("quests/quest")) {
			Quest quest = Quest.fromXMLNode(node);
			quests[quest.id] = quest;
			Debug.Log("- Loaded: " + quest.id + " -" + quest.title);
		}
	}
	
	public static Quest getRandomQuest() {
		return quests.ToList()[randomizer.Next(quests.Count)].Value;
	}
	
}
