﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerController : MonoBehaviour, IDamageable {
	
	protected float throttle = 0; // Intention of forward velocity
	protected float steering = 0; // Intention of rotation / turning
	
	public float speed = 100.0f;
	public float turnSpeed = 35.0f;
	
	public int health = 100;
	public int credits = 0;
	
	public int bodyDecay = 500;
	public bool isAlive = true;
	
	protected Rigidbody2D body;
	protected ParticleSystem splash;
	protected ParticleSystem damageParticles;
	protected WeaponCannon cannon;

	// Use this for initialization
	public void Start () {
		body = GetComponent<Rigidbody2D>();
		splash = GetComponent<ParticleSystem>();
		cannon = GetComponent<WeaponCannon>();
		
		damageParticles = transform.Find("DamageParticles").gameObject.GetComponent<ParticleSystem>();
		damageParticles.emissionRate = 0;
		damageParticles.enableEmission = true;
	}
	
	public void CheckBodyDecay() {
		if(!isAlive) {
			
			if(bodyDecay < 100) {
				damageParticles.Stop();
			}
			
			if(bodyDecay < 150) {
				Destroy(GetComponent<SpriteRenderer>());
			}
			
			if(bodyDecay <= 0) {
				Destroy(gameObject);
				return;
			}
			
			bodyDecay--;
		}
	}
	
	public void TriggerDamage(GameObject source, int damage) {
		health -= damage;
		
		damageParticles.Emit(transform.position, Vector3.zero, 2, 1, Color.white);
		
		Debug.Log("You were hit with " + damage + " HP, health is now " + health + " HP");
		
		if(health <= 0) {
			TriggerDeath(source);
		}
		
		UpdateDamageParticles();
	}
	
	public void UpdateDamageParticles() {
		float rate = (1 - (health / 100.0f)) * 20.0f;

		damageParticles.emissionRate = rate;
		damageParticles.Play();
	}
	
	public void TriggerDeath(GameObject source) {
		Debug.Log("You died!");
		
		isAlive = false;
		
		// Explosion
		damageParticles.Emit(transform.position, Vector3.zero, 6, 5, Color.white);
		damageParticles.Emit(transform.position, Vector3.zero, 3, 4, Color.white);
		damageParticles.Emit(transform.position, Vector3.zero, 4, 4, Color.white);
		damageParticles.Emit(transform.position, Vector3.zero, 6, 4, Color.white);
		
		// Fire
		damageParticles.startSize = 1;
		damageParticles.emissionRate = 40;
		damageParticles.Play();
		
		Destroy(GetComponent<Rigidbody2D>());
		Destroy(GetComponent<Collider2D>());
	}
	
	public void CheckInput() {
		if(Input.GetKeyDown(KeyCode.LeftShift)) {
			cannon.Fire(Vector3.left);
		}
		
		if(Input.GetKeyDown(KeyCode.RightShift)) {
			cannon.Fire(Vector3.right);
		}
	}
	
	public void Update () {
		
		if(!isAlive) return;
		
		steering = -Input.GetAxis("Horizontal");
		throttle = -Input.GetAxis("Vertical");
		
		body.angularVelocity = steering * turnSpeed;
		body.AddForce(throttle * new Vector2((float) Math.Sin(-transform.eulerAngles.z * Mathf.Deg2Rad) * speed, (float) Math.Cos(-transform.eulerAngles.z * Mathf.Deg2Rad) * speed));
		
		CheckInput();
		
	}
	
	public void GiveCredits(int amount) {
		credits += amount;
	}
	
}
