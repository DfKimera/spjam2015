﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
	
	public bool isFiring = false;
	public bool isPlayer = false;
	
	public int damage = 0;
	public int timeToLive = 3000;
	
	public float speed = 0.2f;
	public Vector3 direction;

	// Use this for initialization
	void Start () {
		timeToLive = (int) (timeToLive * speed);
		Physics2D.IgnoreLayerCollision(10, 10, true); // Ignore self collision
	}
	
	// Update is called once per frame
	void Update () {
		if(!isFiring) return;
		
		if(timeToLive <= 0) {
			Destroy(gameObject);
			return;
		}
		
		timeToLive--;
				
		transform.Translate(direction * speed, Space.Self);
		this.GetComponent<ParticleSystem>().emissionRate = 5;
		
	}
	
	public void FireFrom(GameObject source, int damage, Vector3 direction) {
		this.GetComponent<ParticleSystem>().emissionRate = 0;
		
		Physics2D.IgnoreCollision(GetComponent<Collider2D>(), source.GetComponent<Collider2D>());
		
		this.damage = damage;
		
		this.transform.position = source.transform.position;
		this.transform.rotation = source.transform.rotation;
		this.direction = direction;
		
		this.isPlayer = source.CompareTag("Player");
		this.isFiring = true;
		
	}
	
	public void OnCollisionEnter2D(Collision2D other) {
		if(!other.gameObject.CompareTag("Player") && !other.gameObject.CompareTag("Enemy")) {
			Debug.Log("Entity not damageable: " + other + " (tag: " + other.gameObject.tag + ")");
			return; // Not damage-able
		}
		if(isPlayer && other.gameObject.CompareTag("Player")) {
			Debug.Log("Cannot collide on player's own projectile!");
			return; // Player's own projectile
		}
		if(!isPlayer && other.gameObject.CompareTag("Enemy")) {
			Debug.Log("Cannot collide on enemy's own projectile");
			return; // Enemy's own projectile
		}
		
		Debug.Log("Triggering damage on entity: " + gameObject);
		
		other.gameObject.GetComponent<IDamageable>().TriggerDamage(gameObject, damage);
		
	}
}
