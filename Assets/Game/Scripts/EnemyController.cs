﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour, IDamageable {
	
	public int health = 100;
	public bool isAlive = true;
	
	protected int bodyDecay = 500;
	
	protected ParticleSystem damageParticles;

	// Use this for initialization
	public void Start () {
		damageParticles = transform.Find("DamageParticles").gameObject.GetComponent<ParticleSystem>();
		damageParticles.emissionRate = 0;
		damageParticles.enableEmission = true;
	}
	
	// Update is called once per frame
	public void Update () {
		CheckBodyDecay();
	}
	
	public void CheckBodyDecay() {
		if(!isAlive) {
			
			if(bodyDecay < 100) {
				damageParticles.Stop();
			}
			
			if(bodyDecay < 150) {
				Destroy(GetComponent<SpriteRenderer>());
			}
			
			if(bodyDecay <= 0) {
				Destroy(gameObject);
				return;
			}
			
			bodyDecay--;
		}
	}
	
	public void TriggerDamage(GameObject source, int damage) {
		health -= damage;
		
		damageParticles.Emit(transform.position, Vector3.zero, 2, 1, Color.white);
		
		Debug.Log("Enemy was hit with " + damage + " HP, health is now " + health + " HP");
		
		if(health <= 0) {
			TriggerDeath(source);
		}
		
		UpdateDamageParticles();
	}
	
	public void UpdateDamageParticles() {
		float rate = (1 - (health / 100.0f)) * 20.0f;

		damageParticles.emissionRate = rate;
		damageParticles.Play();
	}
	
	public void TriggerDeath(GameObject source) {
		Debug.Log("Enemy has died!");
		
		isAlive = false;
		
		damageParticles.Emit(transform.position, Vector3.zero, 5, 2, Color.white);
		
		damageParticles.startSize = 1;
		damageParticles.emissionRate = 40;
		damageParticles.Play();
		
		Destroy(GetComponent<Rigidbody2D>());
		Destroy(GetComponent<Collider2D>());
		
		GameObject.FindWithTag("Player").GetComponent<PlayerController>().GiveCredits(10);
	}
}
