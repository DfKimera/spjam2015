﻿using UnityEngine;
using System.Collections;

public class CameraToggleZoom : MonoBehaviour {

	public float regularZoom = 10;
	public float minimapZoom = 45;
	
	public bool isMinimap = false;
	protected Camera camera;

	// Use this for initialization
	void Start () {
		camera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Tab)) {
			isMinimap = !isMinimap;
		}
		
		camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, (isMinimap) ? minimapZoom : regularZoom, 0.2f);
	}
}
