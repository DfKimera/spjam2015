using UnityEngine;
using System.Collections;
  
public class AnimatedUVs : MonoBehaviour {
     public int materialIndex = 0;
     public Vector2 uvAnimationRate = new Vector2( 1.0f, 0.0f );
     public string textureName = "_MainTex";
  
     Vector2 uvOffset = Vector2.zero;
  
     void LateUpdate() {
		 
		 Material mat = GetComponent<Renderer>().materials[ materialIndex ];
         uvOffset += ( uvAnimationRate * Time.deltaTime );
		 
         if( GetComponent<Renderer>().enabled ) {
             mat.SetTextureOffset( textureName, uvOffset );
         }
     }
 }