﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

using System;
using System.Collections;
using System.Collections.Generic;

public class QuestManager : MonoBehaviour {
	
	public static QuestManager controller;
	
	public GameObject questPanel;
	protected Animator questPanelControl;
		
	public List<Quest> activeQuests = new List<Quest>();
	public Quest currentlyOpenQuest;

	// Use this for initialization
	void Start () {
		controller = this;
		
		questPanelControl = questPanel.GetComponent<Animator>();
		questPanel.transform.Find("AcceptBtn").gameObject.GetComponent<Button>().onClick.AddListener(delegate {
			EnterQuest(currentlyOpenQuest);
			HideQuestPanel();
		});
		
		questPanel.transform.Find("RejectBtn").gameObject.GetComponent<Button>().onClick.AddListener(delegate {
			HideQuestPanel();
		});
	}
	
	public void ShowRandomQuest() {
		ShowQuest(GameData.getRandomQuest());
	}
	
	public void HideQuestPanel() {
		currentlyOpenQuest = null;
		questPanelControl.SetBool("isHidden", true);
	}
	
	public void EnterQuest(Quest m) {
		//if(activeQuests.Contains(m)) return;
		//activeQuests.Add(m);
	}
	
	public void ShowQuest(Quest m) {
		currentlyOpenQuest = m;
		questPanel.transform.Find("Label").gameObject.GetComponent<Text>().text = m.title;
		questPanel.transform.Find("Description").gameObject.GetComponent<Text>().text = m.description;
		questPanel.transform.Find("Reward").gameObject.GetComponent<Text>().text = Convert.ToString(m.reward);
		
		questPanelControl.enabled = true;
		questPanelControl.SetBool("isHidden", false);
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.M)) {
			ShowRandomQuest();
		}
	}
}
