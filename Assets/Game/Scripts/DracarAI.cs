﻿using UnityEngine;
using System.Collections;

public class DracarAI : MonoBehaviour {
	
	protected EnemyController ctrl;
	protected Rigidbody2D body;
	protected WeaponCannon cannon; 
	
	public bool inPursuit = false;
	public bool isAttacking = false;
	
	public float pursuitSpeed = 130.0f;
	public float attackRange = 10.0f;
	public float pursuitRange = 6.0f;
	public float brakingRatio = -0.55f;
	
	protected GameObject playerInPursuit;

	void Start () {
		ctrl = GetComponent<EnemyController>();
		body = GetComponent<Rigidbody2D>();
		cannon = GetComponent<WeaponCannon>();
	}

	void Update () {
		
		if(!ctrl.isAlive) return;
		
		inPursuit = (playerInPursuit != null);
		
		if(!inPursuit) return;
		if(!playerInPursuit.GetComponent<PlayerController>().isAlive) return;
		
		float distance = Vector2.Distance(transform.position, playerInPursuit.transform.position);
		float pursuitAngle = Vector2.Angle(transform.position, playerInPursuit.transform.position);
		
		Vector2 pursuitThrust = (playerInPursuit.transform.position - transform.position).normalized;
		
		if(distance < attackRange) { // Firing
			cannon.Fire(Vector3.down);
		}
		
		if(distance < pursuitRange) { // Braking
			body.AddForce(brakingRatio * pursuitSpeed * pursuitThrust);
			return;
		}
		
		body.MoveRotation((Mathf.Atan2(pursuitThrust.y, pursuitThrust.x) * Mathf.Rad2Deg) - 90);
		body.AddForce(pursuitSpeed * pursuitThrust);
		
	}
	
	public void OnTriggerEnter2D(Collider2D other) {
		if(other.CompareTag("Player")) {
			Debug.Log("Player has entered pursuit range!");
			playerInPursuit = other.gameObject;
		}
	}
	
	public void OnTriggerExit2D(Collider2D other) {
		if(other.CompareTag("Player")) {
			Debug.Log("Player has left pursuit range!");
			playerInPursuit = null;
		}
	}
}
