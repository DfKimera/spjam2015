using UnityEngine;

interface IDamageable {
	
	void TriggerDamage(GameObject source, int damage);
	
}