﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class QuestGiver : MonoBehaviour {
	
	public int[] questsIdsToGive;
	
	void OnTriggerEnter2D(Collider2D other) {
		if(!other.CompareTag("Player")) return;
		QuestManager.controller.ShowRandomQuest();
	}
	
	void OnTriggerExit2D(Collider2D other) {
		if(!other.CompareTag("Player")) return;
		QuestManager.controller.HideQuestPanel();
	}
}
